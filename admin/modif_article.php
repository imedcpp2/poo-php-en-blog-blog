<?php
    session_start();
    require_once("../partails/header_admin.inc");

  if(  isset($_POST["id_article_modif"]) ){

    $id = $_POST["id_article_modif"];
    require_once("../partails/conect.inc");
    if(!$base)
    {
        header('Location: ../erreur.php');
    }

    $sql="SELECT  titre, article, _url, _date, auteur FROM articles WHERE id = ? " ;
   //echo "<br>".$sql;
    $resultat = mysqli_prepare($base, $sql);
    $ok = mysqli_stmt_bind_param($resultat, 'i',$id);
   // var_dump( $resultat);
    $ok = mysqli_stmt_execute($resultat);
    mysqli_stmt_bind_result($resultat,$titre,$article,$url,$date,$auteur);  
}
  
if(  isset($_POST["titre"]) && isset($_POST["article"])  && isset($_POST["_date"]) && isset($_POST["auteur"]) &&isset($_POST["id_article_modif"]) ){
  if( ( isset($_FILES['url_image']['name']) && ( $_FILES['url_image']['error'] == UPLOAD_ERR_OK) )) {
    $titre=$_POST["titre"];
    $article=$_POST["article"];
    $date=$_POST["_date"];
    $auteur=$_POST["auteur"];
    $url="images/".$_FILES['url_image']['name'];
    $id=$_POST["id_article_modif"];
  } 
    
}
  
?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#modalContactForm').modal('show');
            $(".btn_modal").on('click', function () {
                var i = "#modal";
                i += $(this).next().html();
                $(i).modal('show');
                //alert(i);
            });
            $("#imag_log").on('click', function () {
                //alert("aloo");
                $("#menu").removeClass("invisible");
                $("#menu").slideToggle(500);

            });
        });
    </script>
    <?php     if ( isset($_POST["id_article_modif"]) ){  mysqli_stmt_fetch($resultat);?>
    <div class="container">

        <form action="" method="POST" enctype="multipart/form-data" >
            <div class="form-group">
                <label for="exampleFormControlInput1">Titre d'article</label>
                <input type="text" class="form-control" name="titre" value="<?= $titre; ?>">
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">L'Article</label>
                <textarea class="form-control" rows="6" name="article"><?= $article; ?></textarea>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Date de parution</label>
                <input type="date" class="form-control" value="<?= $date; ?>" name="_date">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Auteur</label>
                <input type="text" class="form-control" name="auteur" value="<?= $auteur; ?>">
            </div>
            <div class="thumbnail">
                <a href=".">
                    <img alt="Lights" style="height:300px;width:100%" src="<?= $url; ?>">
                </a>                
            </div>
            <br/>
            <input type="text" value="<?= $_POST["id_article_modif"];?>" class="hidden"  name="id_article_modif">
            <br>
            <input type="file"  name="url_image">
            <button type="submit" class="btn btn-primary">Valider la modification</button>
        

        </form>
    </div>
    <?php } ?>
    <?php require_once("../partails/footer.inc")?>