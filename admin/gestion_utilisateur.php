<?php
    session_start();
    require_once("../partails/header_admin.inc");
    require_once("../partails/conect.inc");
    $sql="SELECT * FROM contact";
    $resultat = mysqli_query($base, $sql);
    if(!$resultat)
        header('Location: erreur.php');
    
?>
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#imag_log").on('click', function () {
                //alert("aloo");
                $("#menu").removeClass("invisible");
                $("#menu").slideToggle(500);

            });
            $("#message_acceuil").fadeOut("5000");
        });
    </script>

    <div class="container" style="">
        <br><br><br>
        <table class="table table-sm">
            <thead>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Prenom</th>
                    <th scope="col">Mail</th>
                    <th scope="col">Mot de passe</th>
                    <th scope="col">Type</th>
                    <th scope="col">image</th>
                    <th scope="col" colspan="2" class="text-centr">Modifier l'utilisateur</th>
                </tr>
            </thead>
            <tbody>
                <?php  while ( $ligne = mysqli_fetch_array($resultat) ) {?>
                <tr>
                    <td>
                        <?= $ligne[4]; ?>
                    </td>
                    <td>
                        <?= $ligne[5]; ?>
                    </td>
                    <td>
                        <?= $ligne[0]; ?>
                    </td>
                    <td>
                        <?= $ligne[1]; ?>
                    </td>
                    <td>
                        <?= $ligne[2]; ?>
                    </td>
                    <td>
                        <?php $src=substr($ligne[3],2); ?>
                        <img src="<?=$src ?>" alt="Logo" class=" img-circle" id="imag_log_modif" style=" width: 50px; height: 50px;">
                    </td>
                    <td>
                        <form action="" method="POST">
                            <input type="text" value="<?php echo $ligne[0];?>" class="hidden" name="id_article_modif">
                            <button type="submit" class="btn btn-primary">Modidier</button>
                        </form>
                    </td>
                    <td>
                        <form action="" method="POST">
                            <input type="text" value="<?php echo $ligne[0];?>" class="hidden" name="id_article_supp">
                            <button type="submit" class="btn btn-danger" OnClick="confirm('Voulez-vous vraiment supprimer ?');">Supprimer</button>
                        </form>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
	<?php require_once("../partails/footer.inc")?>
    