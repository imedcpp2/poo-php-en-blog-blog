<?php
        session_start();
        if( isset($_POST["id_article_supp"]) )
            echo $_POST["id_article_supp"];
		class DateTimeFrench extends DateTime {
		public function format($format) {
			$english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
			$french_days = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
			$english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'Décember');
			$french_months = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
			return str_replace($english_months, $french_months, str_replace($english_days, $french_days, parent::format($format)));
        }
        
}

 require_once("../partails/header_admin.inc");
 require_once("../donnee.php");
 $DTZ = new DateTimeZone('Europe/Paris');
 ?>

	<script type="text/javascript">
		$(document).ready(function () {
			$('#modalContactForm').modal('show');
			$(".btn_modal").on('click', function () {
				var i = "#modal";
				i += $(this).next().html();
				$(i).modal('show');
				//alert(i);
			});
			$("#imag_log").on('click', function () {
				//alert("aloo");
				$("#menu").removeClass("invisible");
				$("#menu").slideToggle(500);

			});
		});
	</script>
	<div class="container">
		<br>
		<br>
		<br>
		<form action="ajout_article.php" method="POST">                            
                <button type="submit" class="btn btn-primary" >Ajouter un article</button>
        </form>
		<?php while ($ligne = mysqli_fetch_array($resultat) ) { ?>
		<div class="row">

			<div class="col-md-12 text-center">
				<h2 class="bg-primary text-white">
					<?= $ligne[1];?>
				</h2>
				<div class="thumbnail">
					<a href=".">
						<img alt="Lights" style="height:300px;width:100%" src="<?= $ligne[3];?>">
					</a>
				</div>
				<div class="bg-primary text-white text-left">
					<p>
						<?= substr($ligne[2],0,50);?>
					</p>
				</div>
				<div class="row">
					<div class="col-md-8 text-left">
						Paru le :
						<?php  $DT = new DateTimeFrench($ligne[4], $DTZ);echo $DT->format('l j F Y');?>

					</div>
					<div class=" col-md-2 text-right">
						Auteur :
						<?= $ligne[5]?>
					</div>
					<div class=" col-md-2 text-right">
						<button type="button" class="btn btn-primary btn_modal">Lire Plus</button>
						<p style="visibility: hidden">
							<?php echo $ligne[0];?>
						</p>
						<div class="modal fade" id="modal<?php echo $ligne[0];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
						 aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h2 class="modal-title text-left">
											<?php echo $ligne[1];?>
										</h2>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<p>
											<?=$ligne[2];?>
										</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
                            
                    <div class="row">
                        <div class=" col-md-6 text-left">
                        <form action="modif_article.php" method="POST">                            
                        <input type="text" value="<?php echo $ligne[0];?>" class="hidden"  name="id_article_modif">
                            <button type="submit" class="btn btn-primary" >Modidier</button>
                        </form>
                        </div>
                        <div class=" col-md-6 text-left">
                        <form action="" method="POST">
                        <input type="text" value="<?php echo $ligne[0];?>" class="hidden"  name="id_article_supp">
                            <button type="submit" class="btn btn-danger" OnClick="confirm('Voulez-vous vraiment supprimer ?');" >Supprimer</button>
                        </form>
                        </div>
                    </div>
              
			</div>
		</div>

		<br>
		<br>
		<?php } ?>
	</div>



	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<?php require_once("../partails/footer.inc")?>
	</body>
		</html>