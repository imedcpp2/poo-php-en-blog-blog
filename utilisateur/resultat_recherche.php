<?php
	require_once("../partails/autothenfication.inc");
    require_once("../partails/header.inc");
    require_once("../donnee.php");
    require("../Les_classe/utilisateur.class.php");
    $DTZ = new DateTimeZone('Europe/Paris');

    try {
      if(isset($_GET['id']) && $_GET['id']!=""){
        $vla=$_GET['id'];
        $base = new PDO("mysql:host=localhost;dbname=base_01;charset=utf8", "root", "");
        $base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); 
        $articel = new Article($base);
        $response = $articel->resultat_by_id($vla);
      }else if (isset($_POST['zone_recherche']) && $_POST['zone_recherche'] !=""){
          $vla=$_POST['zone_recherche'];
          $base = new PDO("mysql:host=localhost;dbname=base_01;charset=utf8", "root", "");
          $base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); 
          $articel = new Article($base);
          $response = $articel->resultat_by_titre_auteur($vla);
      }else "pas de recupération";
  }
  catch(Exception $e)
  {
  // message en cas d"erreur
  die('Erreur : '.$e->getMessage());
  
  }
  finally{
      $base=NULL;
  }

 ?>
    <style>
        .ui-autocomplete {
            max-height: 150px;
            overflow-y: auto;
            background-color: rgb(9, 186, 240);
            overflow-x: hidden;
        }

        * html .ui-autocomplete {
            height: 100px;
        }
    </style>

    <br>
    <br>
    <br>
    <br>

    <div class="container" style="margin-top: 50px;">
        <?php require_once("../partails/modal.inc"); ?>        
        <table class="table " style="margin-top: 50px;">
            <tbody>
                <tr class="bg-info">
                    <th colspan="3" class="text-center">

                        <h2> Resultat de votre recherhce</h2>
                        <br>
                    </th>
                </tr>              
                <tr>
                    <td colspan="3" class="text-left">
                      <?php if($response->rowCount() != 0 ){ ?>
                        <div id="accordion">
                        <?php while($ligne=$response->fetch()){?>
                            <div class="card">
                                <div class="card-header" id="heading<?=$ligne[0]?>">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?=$ligne[0]?>" aria-expanded="false" aria-controls="collapseTw">
                                            <?=$ligne[1];?>
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapse<?=$ligne[0]?>" class="collapse" aria-labelledby="heading<?=$ligne[0]?>" data-parent="#accordion">
                                    <div class="card-body row">
                                    <div class="col-md-12"> <?=$ligne[2];?></div>
                                    <div class="col-md-12"> <br><br></div>                                    
                                    <div class="col-md-6"><b>Auteur : </b> <?=$ligne[5];?></div>                      
                                    <div class="col-md-6"><b>Paru le : </b> <?php  $DT = new DateTimeFrench($ligne[4], $DTZ);echo $DT->format('l j F Y');?></div>                      
                                    </div>
                                </div>
                            </div>                        
                        <?php } ?>
                        </div>
                      <?php } else  echo "<h4>Ont a rien trouver qui correspondant a votre recherche de : </h4> ".$vla;?>
                    </td>
                </tr>
                
            </tbody>
        </table>
    </div>
    <?php require_once("../partails/footer.inc")?>
    