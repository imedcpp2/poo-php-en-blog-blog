<?php

	require_once("../partails/autothenfication.inc");
    require_once("../partails/header.inc");
    require_once("../donnee.php");
    require("../Les_classe/utilisateur.class.php");
    try {
        $base = new PDO("mysql:host=localhost;dbname=base_01;charset=utf8", "root", "");
        $base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);        
        $mail=$_SESSION["mail"];
        $util = new utilisateur($base);
        if(isset($_POST["nom"]) && isset($_POST["prenom"]) && isset($_POST["pwd"]) ){

            $nom1 =$_POST["nom"];
            $prenom =$_POST["prenom"];
            $pwd =$_POST["pwd"];
              
            
            if ( (isset($_FILES['photo']['name']) && ( $_FILES['photo'] ['error'] == UPLOAD_ERR_OK) ) ){
                
                $photo="../images/logo/".$_FILES["photo"]["name"];
                $chemin_destination="../images/logo/";
                $okmov = move_uploaded_file($_FILES["photo"]["tmp_name"],$chemin_destination.$_FILES["photo"]["name"]);                                
                $okins = $util->modif_utilisateur($mail,$pwd,$nom1,$prenom,$photo);  
                 $_SESSION["url"]=$photo;
                
                if($okmov && $okins ) {
                    echo'<script> $(document).ready(function () { toastr.success("Modifictaion effectuée avec succès...!", "Success", {positionClass: "toast-top-center", escapeHtml:true,"progressBar": true}); });</script>';
                }
                else
                    echo'<script> $(document).ready(function () { toastr.error("On arrive pas à effectuée le modification essayer ultérieurement...!", "Erreur", {positionClass: "toast-top-center", escapeHtml:true,"progressBar": true}); });</script>';    
            }
            else{

                 if($util->modif_utilisateur($mail,$pwd,$nom1,$prenom,"nok")) 
                    echo'<script> $(document).ready(function () { toastr.success("Modifictaion effectuée avec succès...!", "Success", {positionClass: "toast-top-center", escapeHtml:true,"progressBar": true}); });</script>';
                else
                     echo'<script> $(document).ready(function () { toastr.error("On arrive pas a effectuée le modification essayer ultérieurement...!", "Erreur", {positionClass: "toast-top-center", escapeHtml:true,"progressBar": true}); });</script>';
                    

            }
        }

        $tab=$util->revoyer_utilisateur($mail);        
    }
    catch(Exception $e)
    {
    // message en cas d"erreur
    die('Erreur : '.$e->getMessage());

    }
    finally{
        $base=NULL;
    }
    
 ?>
    <style>
        .ui-autocomplete {
            max-height: 150px;
            overflow-y: auto;
            background-color: rgb(9, 186, 240);
            overflow-x: hidden;
        }

        * html .ui-autocomplete {
            height: 100px;
        }
    </style>
    <script>
        $(document).ready(function () {  
             
            toastr.options.onHideonShown = function() { alert("aloo"); };
           

        });

    </script>

    <br>
    <br>
    <br>


    <div class="container" style="">
    <?php require_once("../partails/modal.inc"); ?>
        <br>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-3 mb-5">
                <br>
            </div>
            <div class="col-md-6 mb-5" style="margin-top: 50px;">
                <form action="" method="POST" enctype="multipart/form-data">
                    <table class="table table-sm">
                        <tbody>
                            <tr>
                                <th colspan="2" class="text-center">
                                    <br>
                                    Modification de paramétres de compte
                                </th>
                                <td class="text-center">
                                    <img src="<?php echo $_SESSION["url"]; ?>" alt="Logo" class=" img-circle" style="width: 60px; height: 60px;">
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">
                                    Nom
                                </th>
                                <td colspan="2">
                                    <input type="text" value="<?= $tab[2]?>" id="nom" name="nom" class="form-control form-control-lg" placeholder="Votre nom">
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">
                                    Prenom
                                </th>
                                <td colspan="2">
                                    <input type="text" value="<?= $tab[3]?>"  id="prenom" name="prenom" class="form-control form-control-lg" placeholder="Votre prenom">
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">
                                    Mail
                                </th>
                                <td colspan="2">
                                    <input disabled type="email" value="<?= $tab[0]?>"  id="mail" name="mail" class="form-control form-control-lg" placeholder="Votre mail">
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">
                                    Mot de passe
                                </th>
                                <td>
                                    <input type="password" value="<?= $tab[1]?>"  id="pwd" name="pwd" class="form-control form-control-lg" placeholder="Votre mot de passe">
                                </td>
                                <td class="text-right">
                                <button class="btn btn-default" id="btn_visualiser"><i class="fa fa-eye" id="logo_btn_visualiser"></i></button>                                    
                                <td>
                            </tr>
                            <tr>
                                <th scope="col">
                                    image
                                </th>
                                <td colspan="2">
                                    <input type="file" id="file" name="photo" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-right">
                                    <button class="btn btn-primary" type="submit" id="btn_valider_modif">Valider la Modification</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="md-form">
                        <ul id="liste" style="color : red"></ul>
                    </div>
                </form>
            </div>
            <div class="col-md-3 mb-5">
                <br>
                <br>
            </div>
        </div>
    </div>
    <?php require_once("../partails/footer.inc")?>
    