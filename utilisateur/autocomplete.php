<?php
require("../Les_classe/utilisateur.class.php");
try {
    if (isset($_POST['search']) ){
        $vla=$_POST['search'];
        $response= array();
        $base = new PDO("mysql:host=localhost;dbname=base_01;charset=utf8", "root", "");
        $base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); 
        $articel = new Article($base);
        $response = $articel->resultat_by_article($vla);
        echo json_encode($response);
    }
}
catch(Exception $e)
{
// message en cas d"erreur
die('Erreur : '.$e->getMessage());

}
finally{
    $base=NULL;
}