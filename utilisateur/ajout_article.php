<?php

	require_once("../partails/autothenfication.inc");
    require_once("../partails/header.inc");
    require_once("../donnee.php");
    require("../Les_classe/utilisateur.class.php");
    try {
        $base = new PDO("mysql:host=localhost;dbname=base_01;charset=utf8", "root", "");
        $base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);        
        $Article = new Article ($base);
        
        if(isset($_POST["titre"]) && isset($_POST["article"]) && isset($_POST["date"]) && isset($_POST["auteur"]) ){
            
            
            $titre =$_POST["titre"];
            $article =$_POST["article"];
            $date =$_POST["date"];
            $auteur =$_POST["auteur"];
            if ( (isset($_FILES['photo']['name']) && ( $_FILES['photo'] ['error'] == UPLOAD_ERR_OK) ) ){
                
                $url="../images/".$_FILES["photo"]["name"];
                $chemin_destination="../images/";
                $okmov = move_uploaded_file($_FILES["photo"]["tmp_name"],$chemin_destination.$_FILES["photo"]["name"]);           
                $okins = $Article->nouveau_article($titre,$article,$url,$date,$auteur);            
                if ($okins && $okmov)    
                    echo'<script> $(document).ready(function () { toastr.success("Ajout d\'article effectué avec succées...!", "Success", {positionClass: "toast-top-center", escapeHtml:true,"progressBar": true}); });</script>';
                else
                     echo'<script> $(document).ready(function () { toastr.error("On arrive pas a ajuter votre article essayer ultérieurement...!", "Erreur", {positionClass: "toast-top-center", escapeHtml:true,"progressBar": true}); });</script>';
                    

            }else
                echo'<script> $(document).ready(function () { toastr.error("Choisissez  une image pour votre article...!", "Erreur", {positionClass: "toast-top-center", escapeHtml:true,"progressBar": true}); });</script>';
            
        }       
    }
    catch(Exception $e)
    {
    // message en cas d"erreur
    die('Erreur : '.$e->getMessage());

    }
    finally{
        $base=NULL;
    }
    
 ?>
    <style>
        .ui-autocomplete {
            max-height: 150px;
            overflow-y: auto;
            background-color: rgb(9, 186, 240);
            overflow-x: hidden;
        }

        * html .ui-autocomplete {
            height: 100px;
        }
    </style>
    <script>
        $(document).ready(function () {  
             
            toastr.options.onHideonShown = function() { alert("aloo"); };
           

        });

    </script>

    <br>
    <br>
    <br>


    <div class="container" style="">
    <?php require_once("../partails/modal.inc"); ?>
        <br>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-2 mb-5">
                <br>
            </div>
            <div class="col-md-8 mb-5" style="margin-top: 50px;">
                <form action="" method="POST" enctype="multipart/form-data">
                    <table class="table ">
                        <tbody>
                            <tr class="bg-info">
                                <th colspan="3" class="text-center">
                                    
                                   <h2> Ajout Nouveau Article</h2>
                                    <br>
                                </th>
                                
                            </tr>
                            <tr>
                                <th scope="col">
                                    Titre :
                                </th>
                                <td colspan="2">
                                    <input type="text" size="50" id="titre" name="titre" class="form-control form-control-lg" placeholder="Titre de l'article">
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">
                                    Article :
                                </th>
                                <td colspan="2">
                                <textarea class="form-control" id="article" placeholder="Tappez votre article ici" rows="5" name="article"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">
                                    Auteur :
                                </th>
                                <td colspan="2">
                                    <input type="text" id="auteur" name="auteur" class="form-control form-control-lg" placeholder="Le nom de l'auteur">                                    
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">
                                    Date de parution :
                                </th>
                                <td colspan="2">
                                    <input type="date"   id="date" name="date" class="form-control form-control-lg" placeholder="Votre mot de passe">
                                </td>                                
                            </tr>
                            <tr>
                                <th scope="col">
                                    image
                                </th>
                                <td colspan="2">
                                    <input type="file" id="file" name="photo" class="form-control">
                                </td>
                            </tr>
                        
                            <tr>
                                <td colspan="3" class="text-right">
                                    <button class="btn btn-primary" type="submit" id="btn_valider_modif">Valider la Modification</button>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                    
                </form>
            </div>
            <div class="col-md-2 mb-5">
                <br>
                <br>
            </div>
        </div>
    </div>
    <?php require_once("../partails/footer.inc")?>
    