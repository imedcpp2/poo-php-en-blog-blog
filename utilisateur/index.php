<?php
        
 require_once("../partails/autothenfication.inc");
 require_once("../partails/header.inc");
 require_once("../donnee.php");
require("../Les_classe/utilisateur.class.php"); 
 $DTZ = new DateTimeZone('Europe/Paris');

 ?>
    <style>
        .ui-autocomplete {
            max-height: 150px;
            overflow-y: auto;
            background-color: rgb(9, 186, 240);
            overflow-x: hidden;
        }

        * html .ui-autocomplete {
            height: 100px;
        }
    </style>
    <script>
    $(document).ready(function () { 
        
        <?php if($_SERVER["HTTP_REFERER"]=="http://127.0.0.1/phpnoisil/Projet1/index.php") { ?>
        toastr.info('<?php echo $_SESSION["nom"]." ".$_SESSION["prenom"]?>','Bonjour...!',{positionClass: "toast-top-center"},{timeOut: 3000},{escapeHtml:true}); 
        <?php }?>

    });
    </script>
    <br>
    <br>
    <br>
    <div class="container">
        <?php require_once("../partails/modal.inc"); ?>

        <br>
        <br>
        <br>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-8 text-left">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="../utilisateur/index.php?param=1">Début</a>
                            </li>
                            <?php for($i=1;$i<=$nbr_page;$i++) {?>
                            <li class="page-item">
                                <a class="page-link" href="../utilisateur/index.php?param=<?= ($i)?>">
                                    <?= ($i)?>
                                </a>
                            </li>
                            <?php } ?>
                            <li class="page-item">
                                <a class="page-link" href="../utilisateur/index.php?param=<?= $nbr_page?>">Fin</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-2 text-right">
                     <p style="margin-top:30px">Trié par : </p>
                </div>
                <div class="col-md-2 text-right">
                    <br>
                    <select id="inputState" class="form-control form-control-sm">
                        <option <?php if($order_by === "_date") echo "selected"; ?> value="1">Date croissatte</option>
                        <option <?php if($order_by === "_date DESC") echo "selected"; ?> value="2">Date décroissante</option>
                        <option <?php if($order_by === "titre") echo "selected"; ?> value="3" >Titre</option>
                    </select>
                </div>
                <br>
                <br>
            </div>
            <?php while ($ligne = mysqli_fetch_array($resultat) ) { ?>
            <div class="row">

                <div class="col-md-12 text-center">
                    <h2 class="bg-primary text-white">
                        <?= $ligne[1];?>
                    </h2>
                    <div class="thumbnail">
                        <a href=".">
                            <img alt="Lights" style="height:300px;width:100%" src="<?= $ligne[3];?>">
                        </a>
                    </div>
                    <div class="bg-primary text-white text-left">
                        <p>
                            <?= substr($ligne[2],0,50);?>
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-8 text-left">
                            Paru le :
                            <?php  $DT = new DateTimeFrench($ligne[4], $DTZ);echo $DT->format('l j F Y');?>

                        </div>
                        <div class=" col-md-2 text-right">
                            Auteur :
                            <?= $ligne[5]?>
                        </div>
                        <div class=" col-md-2 text-right">
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModall<?= $ligne[0]?>">Lire Plus</button>

                            <!-- Modal -->
                            <div class="modal fade" id="myModall<?= $ligne[0]?>" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title text-left"><?= $ligne[1]?></h4>
                                        </div>
                                        <div class="modal-body">
                                            <p><?= $ligne[2]?></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-default like" >j'aime <i class="fa fa-thumbs-up"></i></button>
                                            <p class="invisible" ><?= $ligne[0]?></p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <?php } ?>
            <div class="row">
                <div class="col-md-8 text-left">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="../utilisateur/index.php?param=1">Début</a>
                            </li>
                            <?php for($i=1;$i<=$nbr_page;$i++) {?>
                            <li class="page-item">
                                <a class="page-link" href="../utilisateur/index.php?param=<?= ($i)?>">
                                    <?= ($i)?>
                                </a>
                            </li>
                            <?php } ?>
                            <li class="page-item">
                                <a class="page-link" href="../utilisateur/index.php?param=<?= $nbr_page?>">Fin</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-2 text-right">
                     <p style="margin-top:30px">Trié par : </p>
                </div>
                <div class="col-md-2 text-right">
                    <br>
                    <select id="inputState2" class="form-control form-control-sm">
                        <option <?php if($order_by === "_date") echo "selected"; ?> value="1">Date croissatte</option>
                        <option <?php if($order_by === "_date DESC") echo "selected"; ?> value="2">Date décroissante</option>
                        <option <?php if($order_by === "titre") echo "selected"; ?> value="3" >Titre</option>
                    </select>
                </div>
                <br>
                <br>
            </div>
        </div>
                            </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

        <!-------------------------------------------------------------------------------->

        <!-------------------------------------------------------------------------------->
        <?php require_once("../partails/footer.inc")?>
        </body>

        </html>