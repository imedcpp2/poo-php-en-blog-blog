<?php
$is_exist=false;
if(  isset($_POST["mail"]) && isset($_POST["pwd"]) ){

    $mail = $_POST["mail"];
    $pwd = $_POST["pwd"];  
    require_once("partails/conect.inc");
    $sql="SELECT * FROM contact WHERE mail = ? AND pwd = ?";
    $resultat = mysqli_prepare($base, $sql);
    $ok = mysqli_stmt_bind_param($resultat, 'ss',$mail,$pwd);
    
    $ok = mysqli_stmt_execute($resultat);
    mysqli_stmt_bind_result($resultat,$mail,$pwd,$type,$url,$nom,$prenom);    
   if(!$base)
    {
        header('Location: erreur.php');
    }
    if ( mysqli_stmt_fetch($resultat) ){

        $is_exist=true;
        session_start();        
        $_SESSION["mail"]=$mail;
        $_SESSION["pwd"]=$pwd;
        $_SESSION["type"]=$type;
        $_SESSION["url"]=$url;
        $_SESSION["nom"]=$nom;
        $_SESSION["prenom"]=$prenom;
        
        
        if($type =="client")
             header('Location: utilisateur');
        else
             header('Location: admin');     
    }else {
    
       
    }

    mysqli_close($base);

}
?>
<!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Page login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./Style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
        <link rel='stylesheet' id='brandflow-wp-css' href='https://mdbootstrap.com/wp-content/plugins/brandflow-wp/public/css/brandflow-wp-public.css?ver=1.0.0'
            type='text/css' media='all' />
        <link rel='stylesheet' id='contact-form-7-css' href='https://mdbootstrap.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0'
            type='text/css' media='all' />
        <link rel='stylesheet' id='dwqa-style-css' href='https://mdbootstrap.com/wp-content/plugins/dw-question-answer-pro/templates/assets/css/style.css?ver=250420160307'
            type='text/css' media='all' />
        <link rel='stylesheet' id='dwqa-style-rtl-css' href='https://mdbootstrap.com/wp-content/plugins/dw-question-answer-pro/templates/assets/css/rtl.css?ver=250420160307'
            type='text/css' media='all' />
        <link rel='stylesheet' id='woocommerce-layout-css' href='https://mdbootstrap.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.3.2'
            type='text/css' media='all' />
        <link rel='stylesheet' id='woocommerce-smallscreen-css' href='https://mdbootstrap.com/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=3.3.2'
            type='text/css' media='only screen and (max-width: 768px)' />
        <link rel='stylesheet' id='woocommerce-general-css' href='https://mdbootstrap.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=3.3.2'
            type='text/css' media='all' />
        <link rel='stylesheet' id='wsl-widget-css' href='https://mdbootstrap.com/wp-content/plugins/wordpress-social-login/assets/css/style.css?ver=4.9.4'
            type='text/css' media='all' />
        <link rel='stylesheet' id='compiled.css-css' href='https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled.min.css?ver=4.5.0'
            type='text/css' media='all' />
        <link rel='stylesheet' id='style.css-css' href='https://mdbootstrap.com/wp-content/themes/mdbootstrap4/style.css?ver=4.9.4'
            type='text/css' media='all' />
        <link rel='stylesheet' id='dwqa_leaderboard-css' href='https://mdbootstrap.com/wp-content/plugins/dw-question-answer-pro/templates/assets/css/leaderboard.css?ver=4.9.4'
            type='text/css' media='all' />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        <link href="../../css/mdb.min.css" rel="stylesheet">
        <script src="js/javaScript2.js"></script>
        
        <style>
            #title_form {

                z-index: 9999;
                width: 500px;
                height: 70px;
                margin-top: 10px;
                right: 550px;
                position: absolute;
            }
        </style>
        <script>
            $(document).ready(function () {
                
                <?php if( ($is_exist == false) && isset($_POST["mail"]) && isset($_POST["pwd"]) ) { ?>

                    toastr.info('Vous avez pas un compt merci de créer un ','Information.!',{timeOut: 5000},{positionClass: "toast-top-center"});
                                    
                      
                <?php } ?>
                    
            });
                        
            </script>
    </head>

    <body>
        <br><br><br><br>
        <div class="row">
        <div class="col-md-3 mb-5">
            <br><br>
        </div>

		<!-- Grid column -->
		<div class="col-md-6 mb-5" style="margin-top: 200px;">
	
			<div class="card mx-xl-5">
	
				<div class="card-body">
					<!--Header-->
					<div class="form-header deep-blue-gradient rounded card info-color text-center">
						<h3>
							<i class="fa fa-lock"></i> Login</h3>
					</div>
                    <form action="" method="POST">
					<!-- Material input email -->
					<div class="md-form ">
						<i class="fa fa-envelope prefix grey-text"></i>
						<input type="text" id="mail" class=" form-control form-control-lg" placeholder="Votre mail" name="mail">
						
					</div>
	
					<!-- Material input password -->
					<div class="md-form ">
						<i class="fa fa-lock prefix grey-text"></i>
						<input type="password" id="pwd" class="form-control form-control-lg" placeholder="Votre mot de passe"  name="pwd">
					</div>
	
					<div class="text-center mt-4">
                        
                        <button class="btn btn-primary" type="submit" id="btn_valider">Enregister</button>
                    </div>
                    <div class="md-form">
                        <ul id="liste" style="color : red"></ul>
                    </div>
                    
                    </form> 
	
				</div>
	
				<!--Footer-->
				<div class="modal-footer">
					<div class="options font-weight-light">
						<p>Vous avez pas un compt?
							<a href="Sing_up.php">Créé votre compte</a>
						</p>
						<p>Mots de passe
							<a href="renvoyer_pwd.php">oublier?</a>
						</p>
					</div>
				</div>
	
			</div>
	
        </div>
        <div class="col-md-3 mb-5">
            <br><br>
        </div>
	</div>


    </html>