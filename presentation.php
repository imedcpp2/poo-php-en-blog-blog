<?php require_once("partails/header.inc")?> 
<div class="container">
    <h3>Presentation du site</h3>
    <p>
    Notre start-up (entreprise d'insertion spécialisée dans la conception web & logicielle à partir des logiciels libres) propose des stages de développement WEB-PHP-(MY)SQL (types de projet : portails web, portails collaboratifs, intranets type GED, web-scraping & text-mining...etc). 
Vous travaillerez au sein d'une équipe de développeurs dans notre bureau d'Issy les Moulineaux en gestion de projet agile et sous l'encadrement quotidien du directeur. 
Un bon niveau d'autonomie est nécessaire. 

Voici les détails du stage :
Contrat : stage conventionné de 2 mois max (ou plus pour les stagiaires déjà rémunérés) 
Nombre de postes ouverts : 1 à 2 (binômes appréciés). 
Temps de travail : temps complet (ou temps partiel min 28h/ semaine), horaires adaptables 
Lieu de travail : Issy les Moulineaux (possibilité de télétravail partiel) 
Études : min. bac +1 en informatique 
Compétences obligatoires : Php5 Objet, (My)Sql, Jquery/JavaScript/Ajax, Html5, Css3 
Compétences souhaitées : Framework Mvc (CodeIgniter ou Symfony), Css Responsive Web Design (Bootstrap), CMS (WordPress, Spip), Design (Photoshop, Illustrator, Indesign...) 
Avantages offerts : formation qualifiante d'une semaine (sur le thème et à la date de votre choix), adhésion gratuite à vie à une association professionnelle d'informaticiens partenaire (incluant un soutien personnalisé jusqu'à votre 1ere année de vie professionnelle), remboursement des frais de transport à 50%... 
Pour les stagiaires méritants : recommandations au sein de notre important réseau professionnel, possibilité d'embauche à l'issue du stage avec formation certifiante ou diplômante de niveau II ou III en alternance... 

Merci de préciser OBLIGATOIREMENT dans votre candidature :
. votre niveau de connaissance en PHP et éventuellement sur un framework MVC (projets réalisés...) 
. la période (dates début/fin) de votre stage 

Vous êtes intéressé(e) par cette opportunité de stage, merci de postuler rapidement ci dessous
    </p>
  <br><br>
  <br><br>
  <br><br>
  <br><br>
  <br><br>
  <br><br>
  <br><br>
  <br><br>
</div>

<!-------------------------------------------------------------------------------->
<?php require_once("partails/footer.inc")?>
</body>
</html>