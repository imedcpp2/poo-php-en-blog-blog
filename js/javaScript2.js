$(document).ready(function () {
    

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( email );
      }
    $("#pwd,#pwd1").keyup(function(){
        var val=$(this).val() ;               
        if( val.length < 10 || !(val.match(/[A-z]/)) ||  !(val.match(/[A-Z]/)) || !(val.match(/[0-9]/)) ){ // si la chaîne de caractères est inférieure à 5
            $(this).css({ // on rend le champ rouge
                borderColor : 'red',
                color : 'red'
            });
        }
        else{
            $(this).css({ // si tout est bon, on le rend vert
            borderColor : 'green',
            color : 'green'
            });
        }
    });
    $("#btn_valider").on('click', function (e) {
        // e.preventDefault();
        var pwd,mail;
        pwd=$("#pwd").val();
        mail=$("#mail").val();
        
        
        $("li").remove();

        if( pwd == "" ){

            e.preventDefault();
            $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Ont peux pas accepter un mots de passe vide</li>");
            
        
        }
        if(mail == ""){

            e.preventDefault();
            $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Ont peux pas accepter un mail vide</li>");   
        }                     
        
        if(!validateEmail(mail)){

            e.preventDefault();
            $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Mail non valid</li>");
            
        }
        if( pwd.length < 10 || !(pwd.match(/[A-z]/)) ||  !(pwd.match(/[A-Z]/)) || !(pwd.match(/[0-9]/)) ){ 

            e.preventDefault();
            $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Le format de mot de passe non valid Majescul et miniscule et des chiffre de longeur de 10 caractére</li>");            

        }                    

    });


});
