$(document).ready(function () {

  
/*********Modification type de zone de pwd****************************** */

   $("#btn_visualiser").on('click', function(e){
    e.preventDefault();
    $("#logo_btn_visualiser").toggleClass("fa-eye");
    $("#logo_btn_visualiser").toggleClass("fa-eye-slash");

    if($("#logo_btn_visualiser").hasClass("fa-eye"))
       $("#pwd").attr('type','password');
    else
       $("#pwd").attr('type','text');   

});     
/************************************************************************ */

    

        function validateEmail(email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test( email );
          }

        $("#pwd,#pwd1").keyup(function(){

        var val=$(this).val() ;               
        if( val.length < 10 || !(val.match(/[A-z]/)) ||  !(val.match(/[A-Z]/)) || !(val.match(/[0-9]/)) ){
            $(this).css({ borderColor : 'red', color : 'red'});
        }
        else{
            $(this).css({ borderColor : 'green', color : 'green'  });
        }
        });

        $("#btn_valider").on('click', function (e) {
        // e.preventDefault();
        var pwd,pwd1,mail;
        pwd=$("#pwd").val();
        pwd1=$("#pwd1").val();
        mail=$("#mail").val();
        nom=$("#nom").val();
        prenom=$("#prenom").val();
        
        $("li").remove();

        if(nom == "" || prenom == "" || nom.length < 3 || prenom.length < 3){

            e.preventDefault();
            if(nom == "" || nom.length < 3)
                $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Format de nom non valide une chaine de caractére d'au moin de 3 caractéres</li>");
            if(prenom =""|| prenom.length < 3)
                 $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Format de prenom non valide une chaine de caractére d'au moin de 3 caractéres</li>");
            
        
        }

        if(pwd != pwd1 || pwd == "" || pwd1 == "" ){

            e.preventDefault();
            if(pwd != pwd1)
                $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Les deux mots de pass ne sont identique</li>");
            if(pwd=="" || pwd1 == "")
                 $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Ont peux pas accepter un mots de passe vide</li>");
            
        
        }
        if(!validateEmail(mail) || mail == "" ){

            e.preventDefault();
            if(!validateEmail(mail))
                 $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Mail non valid</li>");
            if(mail == "")
                 $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Ont peux pas accepter un mail vide</li>");
            
        
        }
        if( pwd.length < 10 || !(pwd.match(/[A-z]/)) ||  !(pwd.match(/[A-Z]/)) || !(pwd.match(/[0-9]/)) ){ 

            $('ul').append("<li> <i class=\"fa fa-exclamation\"></i> Le format de mot de passe non valid Majescul et miniscule et des chiffre de longeur de 10 caractére</li>");            

        }
        

        });


      /*  $(".btn_modal").on('click', function () {
                var i = "#modal";
                i += parseInt ($(this).next().html());
                $(i).modal('show');
                //alert(i);
        });    */


         $("#imag_log").on('click', function () {
                //alert("aloo");
                if($("#menu").hasClass("invisible"))
                    $("#menu").removeClass("invisible");
                else
                        $("#menu").slideToggle(500);

        });   
});