<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php require_once("CDN.inc"); ?>
    <script src="../js/autocomplate.js"></script>
    <script src="../js/javaScript.js"></script>

    <style>
        #map {
            height: 400px;
            width: 100%;
            border: 1mm #2c2c2e solid;
        }
    </style>
</head>

<body>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Imed-Création</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="acceuil.php">Acceuil</a>
                </li>
                <li>
                    <a href="presentation.php">Presentation</a>
                </li>
                <li>
                    <a href="contact.php">Contact</a>
                </li>
            </ul>
            <form class="navbar-form nav navbar-nav" action="../utilisateur/resultat_recherche.php" method="POST">
                <div class="input-group">
                    <input type="text" style="width: 300px;" class="form-control" placeholder="Tapper ici le nom d'un auteur ou le tire d'un article"
                        id="zone_recherche" name="zone_recherche">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <img src="<?php echo $_SESSION["url"]; ?>" alt="Logo" class=" img-circle" id="imag_log">

    </nav>
    <ul id="menu" class="invisible">
        <li class="active">
            <a href="./gestion_utilisateur.php">
                <i class="fa fa-user"></i>
                <span>Gestin des utilisateurs </span>
            </a>
        </li>

        <li>
            <a href="./gestion_article.php">
                <i class="fa fa-sign-out"></i>
                <span>Gestion des article</span>
            </a>
        </li>
        <li>
            <a href="./login.php">
                <i class="fa fa-sign-out"></i>
                <span>Exite</span>
            </a>
        </li>
        <li>
            <a href="login.html">
                <i class="fa fa-sign-out"></i>
                <span>Exite++</span>
            </a>
        </li>
    </ul>