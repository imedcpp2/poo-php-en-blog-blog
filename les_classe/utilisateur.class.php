<?php

class utilisateur
{

    private $base;

    public function __construct($b)
    {
        $this->base=$b;

    }
    //////////Insertion sans photo
    public function insertion($email,$pwd,$nom,$prenom)
    {
        $sql="INSERT INTO contact(mail, pwd, nom , prenom) VALUES (?,?,?,?)";
        $resultat= $this->base->prepare($sql);
        $resultat->execute(array($email,$pwd,$nom,$prenom));
        
        if($resultat)
           return true;
        else
            return false;
              
    }
    //////////Insertion avec photo
    public function insertion_photo ($email,$pwd,$nom,$prenom,$photo)
    {
        $sql="INSERT INTO contact(mail, pwd, nom , prenom, url) VALUES (?,?,?,?,?)";
        $resultat= $this->base->prepare($sql);
        $resultat->execute(array($email,$pwd,$nom,$prenom,$photo));
        if($resultat)
           return true;
        else
            return false;
    }
    ///////////odif un Utilisateur
    public function modif_utilisateur($email,$pwd,$nom,$prenom,$photo)
    {
        if($photo == "nok"){
            
                       
            $sql="UPDATE contact SET pwd=?,nom=?,prenom=? WHERE mail=?";
            $resultat= $this->base->prepare($sql);
            $resultat->execute(array($pwd,$nom,$prenom,$email));

        }else{
            
            $sql="UPDATE contact SET pwd=?,nom=?,prenom=?, url=? WHERE mail=?";
            $resultat= $this->base->prepare($sql);
            $resultat->execute(array($pwd,$nom,$prenom,$photo,$email));

        }
        if($resultat)
           return true;
        else
            return false;
    }
   ////////////////////////vérif exictance
    public function isExicte($email)
    {
        $sql="SELECT * from contact WHERE mail = ?";
        $resultat= $this->base->prepare($sql);
        $resultat->execute(array($email));
        return $resultat->rowCount();
    }

    //////renvoi les information d'un utilsateur
    public function revoyer_utilisateur($email)
    {
        $tab=array();
        $sql="SELECT * from contact WHERE mail = ?";
        $resultat= $this->base->prepare($sql);
        $resultat->execute(array($email));
        if($ligne = $resultat->fetch()){
            $tab[]=$ligne[0];
            $tab[]=$ligne[1];
            $tab[]=$ligne[4];
            $tab[]=$ligne[5];
            return $tab;            
        }
        return false;
        
    }
    ////////renvoi le pwd
    public function renvoyer_pwd($email){
        
            $sql="SELECT * from contact WHERE mail = ?";
            $resultat= $this->base->prepare($sql);
            $resultat->execute(array($email));
            $ligne = $resultat->fetch();
            return $ligne["pwd"];
        
    }

}
///////////////////////////////classe article*////////////////////////////////////////////////////////////////////////////////
class Article {

    private $base;
    public function __construct($b)
    {
        $this->base=$b;

    }
    public function resultat_by_article($titre){
        
        $titre=strtoupper($titre);
        $reponse = array();        
        $sql="SELECT * FROM articles WHERE  UCASE(titre) LIKE '%".$titre."%' ";
        $resultat= $this->base->query($sql);

        while($ligne=$resultat->fetch()){
            $reponse[]= array("value"=>$ligne['id'],"label"=>$ligne['titre']);;
            
        }
        $sql="SELECT * FROM articles WHERE  UCASE(auteur) LIKE '%".$titre."%' ";
        $resultat2= $this->base->query($sql);

        while($ligne2=$resultat2->fetch()){
            $reponse[]= array("value"=>$ligne2['id'],"label"=>$ligne2['auteur']);;
            
        }
        return $reponse;
    }
    /////////////////////////////////////renvoi tous resultat par raport un titre ou auteur
    public function resultat_by_titre_auteur($titre){
        
        $titre=strtoupper($titre);
        $reponse = array();        
        $sql="SELECT * FROM articles WHERE  UCASE(titre) LIKE '%".$titre."%' OR UCASE(auteur) LIKE '%".$titre."%'";
        $resultat= $this->base->query($sql);
        return $resultat;
    }
    ///////////////////////////////////function de like et dislaike
    public function like_by_id($id,$val){

        $id=(int)$id;
        //$val=(int)$val;  
        //$val=0;             
        $sql="SELECT * FROM articles WHERE  id =".$id;
        $resultat= $this->base->query($sql);
        if($ligne2=$resultat->fetch()){

            $nb_v=$ligne2["nb_vue"];
        }
        if( $val >0 )
            $nb_v++;
        else
            $nb_v--;

        $sql="UPDATE articles SET nb_vue=? WHERE id=?";
        $resultat= $this->base->prepare($sql);
        $resultat->execute(array($nb_v,$id));

    }
    /////////////////////////////////////////////recherche par id
    public function resultat_by_id($id){
        
       
        $sql="SELECT * FROM articles WHERE  id =".$id;
        $resultat= $this->base->query($sql);
        return $resultat;
    }
    /////////////////////////////////////Ajout Article
    public function nouveau_article($titre,$article,$url,$date,$auteur){

        $sql="INSERT INTO articles(titre, article, _url, _date, auteur) VALUES (?,?,?,?,?)";
        $resultat= $this->base->prepare($sql);
        $resultat->execute(array($titre,$article,$url,$date,$auteur));
        if($resultat)
           return true;
        else
            return false;
        


    }
}
////////////////////////////////////////////////////////////////////////////////
class DateTimeFrench extends DateTime {
    public function format($format) {
        $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $french_days = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
        $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'Décember');
        $french_months = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
        return str_replace($english_months, $french_months, str_replace($english_days, $french_days, parent::format($format)));
    }	
}	