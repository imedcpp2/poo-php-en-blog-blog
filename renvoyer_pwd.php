
<?php
require("Les_classe/utilisateur.class.php");
$mail_envoyer=false;
try {
    $base = new PDO("mysql:host=localhost;dbname=base_01;charset=utf8", "root", "");
    $base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);   
    
    if(isset($_POST["nom"]) && isset($_POST["prenom"]) && isset($_POST["mail"]) ){

        $nom =$_POST["nom"];
        $prenom =$_POST["prenom"];
        $mail =$_POST["mail"];      
        $util = new utilisateur($base);
        $nb = $util->isExicte($mail);
        if($nb == 1 ){
            $pwd = $util->renvoyer_pwd($mail);
            $to = $mail;
            $subject = "Récupération de mots de passe";
            $message = "Bonjour, Madame, Monsieur,"."\r\n"."Vous avez demander de récupérer votre mots de passe, votre mots de passe pour accéder à votre compte est : ".$pwd."\r\n".
            "Vous pouvez la modifier depuis votre éspace personel"."\r\n"."Cordialement"."\r\n". "l'équipe de imed-développement"."\r\n"."Ce-ci est un email automatique merci de ne pas répondre";
           $headers = "From: imed.zina1308@gmail.com"."\r\n".
           "Reply-To: webmaster@monsite.com"."\r\n".
           "MIME-Version: 1.0"."\r\n";
          if( mail($to, $subject, $message, $headers))
                $mail_envoyer =true;         
                            
        }
    }       
}
catch(Exception $e)
{
// message en cas d"erreur
die('Erreur : '.$e->getMessage());

}
finally{
    $base=NULL;
}

?>
<!DOCTYPE html>
    <html>

    <head>
    <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sing_up</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./Style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
        <link rel='stylesheet' id='brandflow-wp-css' href='https://mdbootstrap.com/wp-content/plugins/brandflow-wp/public/css/brandflow-wp-public.css?ver=1.0.0'
            type='text/css' media='all' />
        <link rel='stylesheet' id='contact-form-7-css' href='https://mdbootstrap.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0'
            type='text/css' media='all' />
        <link rel='stylesheet' id='dwqa-style-css' href='https://mdbootstrap.com/wp-content/plugins/dw-question-answer-pro/templates/assets/css/style.css?ver=250420160307'
            type='text/css' media='all' />
        <link rel='stylesheet' id='dwqa-style-rtl-css' href='https://mdbootstrap.com/wp-content/plugins/dw-question-answer-pro/templates/assets/css/rtl.css?ver=250420160307'
            type='text/css' media='all' />
        <link rel='stylesheet' id='woocommerce-layout-css' href='https://mdbootstrap.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.3.2'
            type='text/css' media='all' />
        <link rel='stylesheet' id='woocommerce-smallscreen-css' href='https://mdbootstrap.com/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=3.3.2'
            type='text/css' media='only screen and (max-width: 768px)' />
        <link rel='stylesheet' id='woocommerce-general-css' href='https://mdbootstrap.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=3.3.2'
            type='text/css' media='all' />
        <link rel='stylesheet' id='wsl-widget-css' href='https://mdbootstrap.com/wp-content/plugins/wordpress-social-login/assets/css/style.css?ver=4.9.4'
            type='text/css' media='all' />
        <link rel='stylesheet' id='compiled.css-css' href='https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled.min.css?ver=4.5.0'
            type='text/css' media='all' />
        <link rel='stylesheet' id='style.css-css' href='https://mdbootstrap.com/wp-content/themes/mdbootstrap4/style.css?ver=4.9.4'
            type='text/css' media='all' />
        <link rel='stylesheet' id='dwqa_leaderboard-css' href='https://mdbootstrap.com/wp-content/plugins/dw-question-answer-pro/templates/assets/css/leaderboard.css?ver=4.9.4'
            type='text/css' media='all' />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        <link href="../../css/mdb.min.css" rel="stylesheet">
        <script src="js/javaScript.js"></script>
        
        <style>
            #title_form {

                z-index: 9999;
                width: 500px;
                height: 70px;
                margin-top: 10px;
                right: 550px;
                position: absolute;
            }
        </style>
       <script>
            $(document).ready(function () {
                
                <?php if($mail_envoyer == false && isset($_POST["mail"]) ) { ?>
                    
                    toastr.error('Ont arrive pas a vous envoyez un mail merci de réssayez ultérierment.', 'Erreur.!',{timeOut: 5000},{positionClass: "toast-top-center"});
                    
                    
                <?php }   ?>
                <?php if( $mail_envoyer == true && isset($_POST["mail"]) ) { ?>
                    toastr.info('Verifiez votre mail, vous devrez ressevrer un mail avec mot de passe','Félicitation.!',{timeOut: 5000},{positionClass: "toast-top-center"});    
                <?php }  ?>
                    
            });                        
            </script>
    </head>
    <body>
        <br>
        <br>
        <br>
        <br>
        <div class="row">

            <div class="col-md-3 mb-5">
                <br>
                <br>
            </div>

            <!-- Grid column -->
            <div class="col-md-6 mb-5" style="margin-top: 150px;">


                <!-- Material form register -->
                <form action="" method="POST" enctype="multipart/form-data" > 
                    <div class="form-header deep-blue-gradient rounded card info-color text-center">
                        <br>
                        <h3>
                            <i class="fa fa-lock"></i> Récupération de mot de passe </h3>
                    </div>

                    <!-- Material input text -->
                    <div class="md-form">
                        <i class="fa fa-user prefix grey-text"></i>
                        <input type="text" id="nom" name="nom" class="form-control form-control-lg" placeholder="Votre nom">
                    </div>
                    <div class="md-form">
                        <i class="fa fa-user prefix grey-text"></i>
                        <input type="text" id="prenom" name="prenom" class="form-control form-control-lg" placeholder="Votre prenom">
                    </div>

                    <!-- Material input email -->
                    <div class="md-form">
                        <i class="fa fa-envelope prefix grey-text"></i>
                        <input type="email" id="mail" name="mail" class="form-control form-control-lg" placeholder="Votre mail">

                    </div>
                    
                    <div class="text-center mt-4">
                        <button class="btn btn-primary" type="submit" id="btn_valider">Enregister</button>
                    </div>
                    <div class="md-form">
                        <ul id="liste" style="color : red"></ul>
                    </div>


                </form>
                <!-- Material form register -->


            </div>
            <div class="col-md-3 mb-5">
                <br>
                <br>
            </div>
        </div>


    </html>